//Eliza Howard CSE 002 2/15/19 
//Using a random generator to produce a card from a deck of cards

public class CardGenerator{
  public static void main(String args[]){

    //Getting random number from 1 to 52
    int card=(int) (Math.floor(Math.random()*52) +1);
    
    //Initializing identity and suite
    String suite = "";
    String identity = "";
    
    //Finding the suite of the card
    if (card<=13)
      //Suite is a diamond
        suite = "Diamonds";
     //Suite is a club
      else if (card>=13 && card <=26)
        suite = "Clubs";
      //Suite is hearts
      else if (card>=26 && card <=39)
        suite = "Hearts";
     //Suite is spades
      else if (card>=39 && card <=52)
        suite = "Spades";
    
    //Using modulus to set the identity of each card 
    switch (card%13){
      case 0: 
        identity = "King";
        break;
      case 12:
        identity = "Queen";
        break;
      case 11: 
        identity = "Jack";
        break;
      case 10:
        identity = "10";
          break;
      case 9:
        identity = "9";
          break;
      case 8: 
        identity = "8";
        break;
      case 7:
        identity = "7";
        break;
      case 6: 
        identity = "6";
        break;
      case 5:
        identity = "5";
          break;
      case 4:
        identity = "4";
          break;
      case 3:
        identity = "3";
        break; 
      case 2:
        identity = "2";
          break;
      case 1:
        identity = "Ace";
        break; 
    }
//Printing out the identity and suite of the random card picked
   System.out.println("You picked a " + identity + " of " + suite + "");
    }

    
  }