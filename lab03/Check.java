//Eliza Howard CSE02 Spring 2019
//Finding the amount each person at a dinner should pay on a check including the tip
import java.util.Scanner;
public class Check{
  public static void main(String args[]){
//setting up an input
Scanner myScanner;
myScanner = new Scanner( System.in );

//Getting the input of the cost of the check
System.out.print("Enter the original cost of the check in the form xx.xx: ");
double checkCost = myScanner.nextDouble();

//Getting the percentage of tip wanted to pay on check
System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):" );
double tipPercent = myScanner.nextDouble();
//Converting tip number to decimal
tipPercent /= 100; 

//Getting number of people that are splitting the check
System.out.print("Enter the number of people who went out to dinner:");
int numPeople = myScanner.nextInt();

//Inputting values
double totalCost;
double costPerPerson;
int dollars, dimes, pennies;
    
//Calculating the total cost
totalCost = checkCost * (1 + tipPercent);
//Calculating the cost per person
costPerPerson = totalCost / numPeople;

//changing the values to the right decimal places
dollars=(int)costPerPerson;
dimes=(int)(costPerPerson * 10) % 10;
pennies=(int)(costPerPerson * 100) % 10;

//printing the cost per person in dollars and cents
System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);


//Eliza Howard CSE02 Spring 2019
//Finding the amount each person at a dinner should pay on a check including the tip
      }   
 
}