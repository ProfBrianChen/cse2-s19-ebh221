//Eliza Howard CSE 002 4/16/19
//

//importing 
import java.util.Random;
import java.util.Arrays;
import java.util.Scanner; 

//public class  
public class ArrayGames{
  public static void main(String args[]){
   
    //random number generation
    Random randomGenerator=new Random();
    //number between 10 and 12 for length
    int length=randomGenerator.nextInt(20-10)+10;
    int [] input1;
    input1= new int [length];
    input1=generate(length);
    //printing input 1
    print(input1);
    
    //number between 10 and 12 for length
    int length2=randomGenerator.nextInt(20-10)+10;
    int [] input2;
    input2= new int [length2];
    input2=generate(length2);
    //printing input 2
    print(input2);
    
    
    
    //asking if they want to shorten or insert
    Scanner myScanner;
    myScanner = new Scanner( System.in );
    System.out.print("Do you want to shorten or insert? Enter 1 for shorten or 0 for insert: ");
    int number1=myScanner.nextInt();
    
    //shorten the array
    if (number1==1){
      //asking for the number for array 1
    System.out.print("What number for array 1:");
      //new array shortened
    int shortnum1=myScanner.nextInt();
    int [] arrayshort1;
    arrayshort1=new int [length];
      //shortened array
    arrayshort1=shorten(input1, shortnum1, length);
      //asking for number for array 2
    System.out.print("What number for array 2:");
      //new array shortened
    int shortnum2=myScanner.nextInt();
    int [] arrayshort2;
    arrayshort2=new int [length2];
      //shorten array
    arrayshort2=shorten(input2, shortnum2, length2);
      //printing arrays
    System.out.println(Arrays.toString(arrayshort1)); 
    System.out.println(Arrays.toString(arrayshort2));
    }
    //inserting arrays
    else if (number1==0){
      //making combo array
    int [] arraycombo;
    arraycombo=new int [length+length2];
      //inserting arrays
    arraycombo=insert(input1, input2, length, length2);
      //printing array
    System.out.println(Arrays.toString(arraycombo));
    }
  //end main method
  }
  
  //generate method
  //creates an array with length 10 to 20
  public static int [] generate(int length){
    Random randomGenerator=new Random();
    //new array
    int[] array;
    //length of random number
    array = new  int [length];
    
    //setting numbers for array from 0 to 99
    for (int i=0; i<length; i++){
      array[i]=randomGenerator.nextInt(99);
    }
    
    //return array
    return array;
    //end generate class
  }
  
  //print method
  //prints out the array
  public static void print(int [] array){ 
    //printing out array
    System.out.println(Arrays.toString(array));
    //return array
    return;
    //end print method
  }
  
  //insert array
  //combines two arrays
  public static int [] insert(int [] input1, int [] input2, int length, int length2){
    //combo array
    int [] combo;
    int combolength=length+length2+1;
    combo = new int [combolength];
    
    //scanning for value
    Scanner myScanner;
    myScanner = new Scanner( System.in );
    
    //asking for what value to be inserted
    System.out.print("How much of the first array do you want to start the new array? Enter an int: ");
    int number=myScanner.nextInt();
    
    //making the array
    int j=0;
    int k=number;
    for (int i=0; i<combolength; i++){
      //for the first couple values
      if (i<=number-1){
        combo[i]=input1[i];
      }
      //inserting array2 after the first couple values
      else if (number<i && i<=(number+length2)) {
        
        combo[i]=input2[j];
        j++;
      }
      //inserting the rest of values
      else if (number+length2<i && i<=(combolength)){
        combo[i]=input1[k];
        k++;
      }
    }
     //returning array
   return combo;
    //end insert method
 }
  
  //shorten method
  public static int [] shorten(int [] array, int number, int length){
  //if the number to shorten is bigger than the length of the array
    if (number>length){
     return array;
   }
    //shortening the array
   else { 
    int [] shorter;
    shorter =new int [length];
    //creating new shortened array
    for(int i=0; i<length; i++){
      //skipping the shortened value
      if (i==number){
        continue;
      }
      //creating the array 
      else {
        shorter[i]=array[i];
      }
    }
    return shorter; 
   }
    
    //end shorten method
  }
//end class
}
//Eliza Howard CSE 002 4/16/19
//