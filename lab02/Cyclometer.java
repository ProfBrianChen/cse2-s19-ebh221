//Eliza Howard February 1st 2019 CSE02 
//Bicycle Cyclometer
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
    int secsTrip1=480;  //Seconds for trip 1
   	int secsTrip2=3220;  //Seconds for trip 2
		int countsTrip1=1561;  //Rotations for trip 1
		int countsTrip2=9037; //Rotation for trip 2
    double wheelDiameter=27.0;  //Wheel diameter 
  	double PI=3.14159; //Pi 
  	int feetPerMile=5280;  //Feet per mile
  	int inchesPerFoot=12;   //Inches per foot
  	int secondsPerMinute=60;  //Seconds per minute
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute) + " minutes and had "+
       	      countsTrip1+" counts.");
      //Prints minutes per trip 1
	  System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
      //Prints minutes per trip 2
    // run the calculations; store the values. Document your
		//calculation here. What are you calculating?
		//I am calculating the distance of each trip in miles using wheel diameter, rotations, time, and some known values 
		//
	double distanceTrip1 = countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
  distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
	double distanceTrip2 = countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //distance of trip 2 in miles
	double totalDistance = distanceTrip1+distanceTrip2; //total distance of both trips
//Print out the output data.
  System.out.println("Trip 1 was " + distanceTrip1 + " miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");

	}  //end of main method   
} //end of class
