//Eliza Howard CSE 002 2/19/19 
//Finding 

import java.util.*;
import java.util.Arrays;
import java.util.List;

public class PokerHandCheck{
  public static void main(String args[]){

    //Getting random number from 1 to 52
   int card1A=(int) (Math.floor(Math.random()*52) +1);
   int card2A=(int) (Math.floor(Math.random()*52) +1);
   int card3A=(int) (Math.floor(Math.random()*52) +1);
   int card4A=(int) (Math.floor(Math.random()*52) +1);
   int card5A=(int) (Math.floor(Math.random()*52) +1);
    
    //initializing the suites
    String suite1="";
    String suite2="";
    String suite3="";
    String suite4="";
    String suite5="";
    
    
    //initializing the identities
    String identity1 = "";
    String identity2= "";
    String identity3 = "";
    String identity4 = "";
    String identity5 = "";
    
    
    
    
    //intitializing the hand
    String hand;
    
    
    //Setting suite of card 1
   if (card1A<=13)
      //Suite is a diamond
        suite1 = "Diamonds";
     //Suite is a club
      else if (card1A>=13 && card1A <=26)
        suite1 = "Clubs";
      //Suite is hearts
      else if (card1A>=26 && card1A <=39)
        suite1 = "Hearts";
     //Suite is spades
      else if (card1A>=39 && card1A <=52)
        suite1 = "Spades";
       
    //Setting suite of card 2
    if (card2A<=13)
      //Suite is a diamond
        suite2 = "Diamonds";
     //Suite is a club
      else if (card2A>=13 && card2A <=26)
        suite2 = "Clubs";
      //Suite is hearts
      else if (card2A>=26 && card2A <=39)
        suite2 = "Hearts";
     //Suite is spades
      else if (card2A>=39 && card2A <=52)
        suite2 = "Spades";
   
    //Setting suite of card 3
       if (card3A<=13)
      //Suite is a diamond
        suite3 = "Diamonds";
     //Suite is a club
      else if (card3A>=13 && card3A <=26)
        suite3 = "Clubs";
      //Suite is hearts
      else if (card3A>=26 && card3A <=39)
        suite3 = "Hearts";
     //Suite is spades
      else if (card3A>=39 && card3A <=52)
        suite3 = "Spades";
    
    
    //Setting suite of card 4
       if (card4A<=13)
      //Suite is a diamond
        suite4 = "Diamonds";
     //Suite is a club
      else if (card4A>=13 && card4A <=26)
        suite4 = "Clubs";
      //Suite is hearts
      else if (card4A>=26 && card4A <=39)
        suite4 = "Hearts";
     //Suite is spades
      else if (card4A>=39 && card4A <=52)
        suite4 = "Spades";
    

    //setting suite of card 5
       if (card5A<=13)
      //Suite is a diamond
        suite5 = "Diamonds";
     //Suite is a club
      else if (card5A>=13 && card5A <=26)
        suite5 = "Clubs";
      //Suite is hearts
      else if (card5A>=26 && card5A <=39)
        suite5 = "Hearts";
     //Suite is spades
      else if (card5A>=39 && card5A <=52)
        suite5 = "Spades";
    
    
    
    //Identifying Card1
  switch (card1A%13){
      case 0: 
        identity1 = "King";
        break;
      case 12:
        identity1 = "Queen";
        break;
      case 11: 
        identity1 = "Jack";
        break;
      case 10:
        identity1 = "10";
          break;
      case 9:
        identity1 = "9";
          break;
      case 8: 
        identity1 = "8";
        break;
      case 7:
        identity1 = "7";
        break;
      case 6: 
        identity1 = "6";
        break;
      case 5:
        identity1 = "5";
          break;
      case 4:
        identity1 = "4";
          break;
      case 3:
        identity1 = "3";
        break; 
      case 2:
        identity1 = "2";
          break;
      case 1:
        identity1 = "Ace";
        break;   
  }
          //Identifying Card2
  switch (card2A%13){
      case 0: 
        identity2 = "King";
        break;
      case 12:
        identity2 = "Queen";
        break;
      case 11: 
        identity2 = "Jack";
        break;
      case 10:
        identity2 = "10";
          break;
      case 9:
        identity2 = "9";
          break;
      case 8: 
        identity2 = "8";
        break;
      case 7:
        identity2 = "7";
        break;
      case 6: 
        identity2 = "6";
        break;
      case 5:
        identity2 = "5";
          break;
      case 4:
        identity2 = "4";
          break;
      case 3:
        identity2 = "3";
        break; 
      case 2:
        identity2 = "2";
          break;
      case 1:
        identity2 = "Ace";
        break;  
  }
          //Identifying Card3
  switch (card3A%13){
      case 0: 
        identity3 = "King";
        break;
      case 12:
        identity3 = "Queen";
        break;
      case 11: 
        identity3 = "Jack";
        break;
      case 10:
        identity3 = "10";
          break;
      case 9:
        identity3 = "9";
          break;
      case 8: 
        identity3 = "8";
        break;
      case 7:
        identity3 = "7";
        break;
      case 6: 
        identity3 = "6";
        break;
      case 5:
        identity3 = "5";
          break;
      case 4:
        identity3 = "4";
          break;
      case 3:
        identity3 = "3";
        break; 
      case 2:
        identity3 = "2";
          break;
      case 1:
        identity3 = "Ace";
        break;  
  }
          //Identifying Card4
  switch (card4A%13){
      case 0: 
        identity4 = "King";
        break;
      case 12:
        identity4 = "Queen";
        break;
      case 11: 
        identity4 = "Jack";
        break;
      case 10:
        identity4 = "10";
          break;
      case 9:
        identity4 = "9";
          break;
      case 8: 
        identity4 = "8";
        break;
      case 7:
        identity4 = "7";
        break;
      case 6: 
        identity4 = "6";
        break;
      case 5:
        identity4 = "5";
          break;
      case 4:
        identity4 = "4";
          break;
      case 3:
        identity4 = "3";
        break; 
      case 2:
        identity4 = "2";
          break;
      case 1:
        identity4 = "Ace";
        break;  
  }
          //Identifying Card1
  switch (card5A%13){
      case 0: 
        identity5 = "King";
        break;
      case 12:
        identity5 = "Queen";
        break;
      case 11: 
        identity5 = "Jack";
        break;
      case 10:
        identity5 = "10";
          break;
      case 9:
        identity5 = "9";
          break;
      case 8: 
        identity5 = "8";
        break;
      case 7:
        identity5 = "7";
        break;
      case 6: 
        identity5 = "6";
        break;
      case 5:
        identity5 = "5";
          break;
      case 4:
        identity5 = "4";
          break;
      case 3:
        identity5 = "3";
        break; 
      case 2:
        identity5 = "2";
          break;
      case 1:
        identity5 = "Ace";
        break;  
  } 
    
    int card1=card1A%13;
    int card2=card2A%13;
    int card3=card3A%13;
    int card4=card4A%13;
    int card5=card5A%13;
    
//Setting all the possibilities for the cards
                                       //Two Pair
    //one of the pairs is 1 and 2
if (card1==card2 && card3==card4)
  hand ="Two Pair";
    else if (card1==card2 && card3==card5)
  hand ="Two Pair";
    else if (card1==card2 && card4==card5)
  hand ="Two Pair";
    //one of the pairs is 1 and 3
    else if (card1==card3 && card2==card4)
  hand ="Two Pair";
    else if (card1==card3 && card2==card5)
  hand ="Two Pair";
    else if (card1==card3 && card4==card5)
  hand ="Two Pair";
    //one of the pairs is 1 and 4
    else if (card1==card4 && card2==card3)
  hand ="Two Pair";
    else if (card1==card4 && card2==card5)
  hand ="Two Pair";
    else if (card1==card4 && card3==card5)
  hand ="Two Pair";
    //one of the pairs is 1 and 5
    else if (card1==card5 && card2==card3)
  hand ="Two Pair";
    else if (card1==card5 && card2==card4)
  hand ="Two Pair";
    else if (card1==card5 && card3==card4)
  hand ="Two Pair";
   //neither pairs have a 
    else if (card2==card3 && card4==card5)
  hand ="Two Pair";
    else if (card2==card4 && card3==card5)
  hand ="Two Pair";
    else if (card2==card5 && card3==card4)
  hand ="Two Pair";
                                   //Three of a Kind
    //Setting the pairs if one of the cards in the three of a kind is card 1
    else if (card1==card2 && card1==card3 && card1!=card4 && card1!=card5)
  hand= "Three of a kind";
    else if (card1==card2 && card1==card4 && card1!=card3 && card1!=card5)
  hand= "Three of a kind";
    else if (card1==card2 && card1==card5 && card1!=card3 && card1!=card4)
  hand= "Three of a kind";
     else if (card1==card3 && card1==card4 && card1!=card2 && card1!=card5)
  hand= "Three of a kind";
    else if (card1==card3 && card1==card5 && card1!=card4 && card2!=card2)
  hand= "Three of a kind";
    else if (card1==card4 && card1==card5 && card1!=card3 && card1!=card2)
  hand= "Three of a kind";
    //Setting the pairs if one of the cards in the three of a kind is card 2
     else if (card2==card3 && card2==card4 && card2!=card1 && card2!=card5)
  hand= "Three of a kind";
     else if (card2==card3 && card2==card5 && card2!=card4 && card2!=card1)
  hand= "Three of a kind";
     else if (card2==card4 && card2==card5 && card2!=card1 && card2!=card3)
  hand= "Three of a kind";
    //Setting the pairs if one of the cards in the three of a kind is card 3
    else if (card3==card4 && card3==card5 && card3!=card1 && card3!=card2)
  hand= "Three of a kind";
                              //Pairs
    //Setting the Pairs if one of the cards in the pair is card1 
    else if (card1==card2 && card1!=card3 && card1!=card4 && card1!=card5)
  hand= "Pair";
    else if (card1==card3 && card1!=card2 && card1!=card4 && card1!=card5)
  hand= "Pair";
    else if (card1==card4 && card1!=card2 && card1!=card3 && card1!=card5)
  hand= "Pair";
     else if (card1==card5 && card1!=card2 && card1!=card3 && card1!=card4)
  hand= "Pair";
    //Setting the Pairs if one of the cards in the pair is card2
    else if (card2==card3 && card2!=card1 && card2!=card4 && card2!=card5)
  hand= "Pair";
    else if (card2==card4 && card2!=card1 && card2!=card3 && card2!=card5)
  hand= "Pair";
     else if (card2==card5 && card2!=card1 && card2!=card3 && card2!=card4)
  hand= "Pair";
    //Setting the pairs if one of the cards in the pair is card 3
     else if (card3==card4 && card3!=card1 && card3!=card2 && card3!=card5)
  hand= "Pair";
     else if (card3==card5 && card3!=card1 && card2!=card3 && card3!=card4)
  hand= "Pair";
    //Setting the pairs if one of the cards in the pair is card 4
    else if (card4==card5 && card4!=card1 && card2!=card4 && card3!=card4)
  hand= "Pair";

//if its none of the above conditions 
    else
  hand= "High Hand";
   
    

//Printing out the identity and suite of the random card picked
    
   System.out.println("The random cards you picked were:");
   System.out.println("" + identity1 + " of " + suite1 + " ");
   System.out.println("" + identity2 + " of " + suite2 + "");
   System.out.println("" + identity3 + " of " + suite3 + "");
   System.out.println("" + identity4 + " of " + suite4 + "");
   System.out.println("" + identity5 + " of " + suite5 + "");
 //Printing out the type of hand  
   System.out.println("You got a " + hand + "!");
    
    
    }

    
  }