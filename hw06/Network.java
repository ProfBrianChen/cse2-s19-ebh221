//Eliza Howard Homework 6 3/19/19 CSE02 
//Displaying a window of network boxes


//importing scanner
import java.util.Scanner; 
  
//public class  
public class Network{
  public static void main(String args[]){
    
    //Scanner input
    Scanner myScanner;
    myScanner = new Scanner( System.in );
    
    //Asking for height
    System.out.print("Input an integer for the height of the window: ");
    int height=myScanner.nextInt();
    
    //asking for width
    System.out.print("Input an integer for the width of the window: ");
    int width=myScanner.nextInt();
    
    //asking for side length
    System.out.print("Input an integer for side length of the box: ");
    int sidelength=myScanner.nextInt();
    
    //asking for edge length
    System.out.print("Input the length of an edge:");
    int edge=myScanner.nextInt();
    
    //setting the number of boxes
    int numberboxesdown=height/sidelength;
    int numberboxesacross=width/sidelength;

    //for even sidelengths
  if (sidelength%2==0){
    //number of boxes down for the whole system
    for (int v=0; v<numberboxesdown; v++){
      //number boxes across for the top row
    for (int m=0; m<numberboxesacross; m++){
      //length of the top of the box
      for(int j=1; j<=sidelength; j++){
        //setting the paramaters for sides of the box
        if(j==1){
          System.out.print("#");}
        else if(j==sidelength){
          System.out.print("#");
        }
        else{
          System.out.print("-");}
      }
        //setting the space for the edges
        for(int p=0; p<edge; p++){
          System.out.print(" ");
        }
      
    }
      //new line after top line
    System.out.print("\n");
    
      //setting the amount of lines that need to be printed
    for (int s=0; s<sidelength; s++){
      //setting the number of patterns across
     for (int g=0; g<numberboxesacross; g++){
       //the size of the pattern
      for (int h=0; h<sidelength; h++){
        //paramters for the pattern
        if(h==0)
          System.out.print("|");
        else if (h==sidelength-1)
          System.out.print("|");
        else 
          System.out.print(" ");
      }
       //edge paramaters
        for(int y=0; y<edge; y++){
          //setting edge lines in middle
          if (s==sidelength/2 && g<numberboxesacross-1)
            System.out.print("=");
          //normal spacing
          else
          System.out.print(" ");
        }
      }
      //printing a new line
    System.out.print("\n");
    }
    
      //number of times the pattern repeats on bottom
     for (int m=0; m<numberboxesacross; m++){
       //size of pattern
      for(int j=1; j<=sidelength; j++){
        //parameters for pattern
        if(j==1){
          System.out.print("#");}
        else if(j==sidelength){
          System.out.print("#");
        }
        else{
          System.out.print("-");}
      }
        //edge length size
        for(int p=0; p<edge; p++){
            System.out.print(" ");            
        }
     }
      //printing out new line
   System.out.print("\n");   
  }
  }  
    
    //for odd side lengths
    else {
    
    //for the whole system down
  for (int m=0; m<numberboxesdown; m++){
    //for number of time pattern repeats across top
    for (int l=0; l<numberboxesacross; l++){ 
      //for size of pattern
    for(int j=1; j<sidelength; j++) {
       //for the pounds sign to start  
       if (j==1)
         System.out.print("#");
       //the dashes in the middle
       else 
       System.out.print("-");
    } 
    }
    System.out.print("#");
    //new line
    System.out.print("\n");
    
    //number of lines down
    for(int u=2; u<sidelength; u++){
      //number of patterns across
    for (int l=0; l<numberboxesacross; l++){
      //size of pattern
    for(int k=1; k<sidelength; k++) {
       //for the dash on the side
       if (k==1)
         System.out.print("|");
      //for the spaces in the middle
       else 
     System.out.print(" ");
     } 
      }
   System.out.print("|");
      //new line
   System.out.print("\n");
    }
    }
  //number of times pattern repeats
  for(int h=0; h<numberboxesacross; h++){
    //size of pattern
    for(int j=1; j<sidelength; j++) {
       //for the pounds sign to start  
       if (j==1)
         System.out.print("#");
       //the dashes in the middle
       else 
       System.out.print("-");
    } 
  }
    System.out.print("#");
      //new line
    System.out.print("\n");
    
    }
//Brackets from opening statement
  }
}
//Eliza Howard Homework 6 3/19/19 CSE02 
//Displaying a window of network boxes
    
    
    