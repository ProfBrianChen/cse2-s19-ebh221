//Eliza Howard Lab 08 4/5/19 CSE02 
//Shuffling arrays


//import java.util.Arrays;
import java.util.Random;
import java.util.Arrays;
import java.lang.Math;

//public class  
public class Lab08{
  public static void main(String args[]){
  
    
    Random randomGenerator=new Random();
    int arraynum=randomGenerator.nextInt(100-50)+50;
    
    System.out.println("The size of the array is " +arraynum);
    
    int[] array;
    array = new  int [arraynum];
    
    for (int i=0; i<arraynum; i++){
      array[i]=randomGenerator.nextInt(99);
      System.out.print(array[i]+ " ");
    }
    
    int range=getRange(array, arraynum);
    
    System.out.print("\n");
    System.out.println("Range is " +range);
    
    int mean=getMean(array, arraynum);
    System.out.println("Mean is " +mean);
    
    
    double stddev=getStdDev(array, arraynum, mean);
    System.out.println("Standard Deviation is " +stddev);
    
    int [] shuffled=shuffle(array, arraynum);
    System.out.println("Shuffled Array is " +Arrays.toString(shuffled));
  }

  
  public static int getRange(int[] array, int length){
    Arrays.sort(array);
    int range=array[length-1]-array[0]; 
    return range;
  }
  
    public static int getMean(int[] array, int length){
    
      int sum=0;
    for (int i=0; i<length; i++){
      sum+=array[i];
    }
  
    int mean=sum/length;
      
    return mean;
  }
  
  

    public static double getStdDev(int[] array, int length, int mean){
        int sum=0;
      
    for (int i=0; i<length; i++){
      int add=array[i]+mean;
      int square=0;
      square+=add*add;
      sum+=square;
    }
      int num=sum/(2*(length-1));
      double stddev=Math.sqrt(num);
      
    return stddev;  
  }
  
  //////////  WHAT EVEN IS THIS
  public static int [] shuffle(int [] array, int length){
    Random randomGenerator=new Random();
    int[] array2;
    array2 = new  int [length];
    
    for (int i=0; i<length; i++){
    array2[i]=randomGenerator.nextInt(length);
    }
   
    return array2;
  }
  
}
  