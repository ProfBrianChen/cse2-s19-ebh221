//Eliza Howard Lab 05 3/1/19 CSE02 
//Making a twist the length that a user is prompted for


//importing scanner
import java.util.Scanner; 
  
//public class  
public class TwistGenerator{
  public static void main(String args[]){
    
    //Scanner input
    Scanner myScanner;
    myScanner = new Scanner( System.in );
    
//Asking for positive integer    
    System.out.print("Enter Positive Interger Length:  ");
    
    //Removing non integer values 
    while (myScanner.hasNextInt()==false) {
      //removing stuff that isnt an int from the loop
      String junkword=myScanner.next();  
      //reprompting for scanner value
      System.out.print("Enter Positive Interger Length:  ");
    }
    
    //Setting int value to integer gotten from while loop
    int length=myScanner.nextInt();
    
    while (length<0) {
      System.out.print("Enter Positive Interger Length:  ");
      length = myScanner.nextInt();
      if (length>0){
        break;
      }
    }
   
      for(int j=0; j<length; j++) {
        if (j%3==1){
          System.out.print("/");
        }
        else if (j%3==2){
          System.out.print(" ");
        }
        else if (j%3==0){
          System.out.print("\\");
        } 
      }
    System.out.println(" ");
     
        
      for(int i=0; i<length; i++) {
       if (i%3==1){
          System.out.print(" ");
        }
        else if (i%3==2){
          System.out.print("X");
        }
        else if (i%3==0){
          System.out.print(" ");
        } 
        }
    System.out.println(" ");

   for(int k=0; k<length; k++) {
       if (k%3==1){
          System.out.print("\\");
        }
        else if (k%3==2){
          System.out.print(" ");
        }
        else if (k%3==0){
          System.out.print("/");
        } 
        }
    System.out.println(" ");
 
    
      
      
    }  
  }