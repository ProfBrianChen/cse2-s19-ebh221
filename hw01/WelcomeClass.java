///
//CSE 02 Welcome Class Homework 1

public class WelcomeClass{
  public static void main(String args[]){
   //Prints a welcome message to terminal 
  System.out.println("-----------");
  System.out.println("| WELCOME |");
  System.out.println("-----------");  
  System.out.println(" ^  ^  ^  ^  ^  ^");
  System.out.println("/ \\ / \\ / \\ / \\ / \\");
  System.out.println("<-E--B--H--2--2--1->");
  System.out.println("\\ / \\ / \\ / \\ / \\ /");
  System.out.println(" v  v  v  v  v  v");
  }   
 
}