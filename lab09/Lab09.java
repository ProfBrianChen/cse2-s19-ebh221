//Eliza Howard Homework 6 3/19/19 CSE02 

import java.util.Random;
import java.util.Arrays;
import java.util.Scanner;


public class Lab09{
  
  public static void main(String args[]){
    
    Scanner myScanner;
    myScanner = new Scanner( System.in );
    System.out.print("Do you want to run a binary or linear search? Enter 0 for linear and 1 for binary: ");
    int searchtype=myScanner.nextInt();
    
    if(searchtype==0){
      System.out.print("Input the desired length of the array: ");
      int arraylength=myScanner.nextInt();
      int [] array;
      array = new int [arraylength];
      array=random(arraylength);
      
      System.out.print("What term do you want to search for? Enter value: ");
      int searchval=myScanner.nextInt();
      int indexval=linearsearch(array, searchval, arraylength);
      System.out.println(Arrays.toString(array));
      System.out.println(indexval);
    }
    else if (searchtype==1){
      System.out.print("Input the desired length of the array: ");
      int arraylength2=myScanner.nextInt();
      int [] array2;
      array2 = new int [arraylength2];
      array2=ascending(arraylength2);
      
      System.out.print("What term do you want to search for? Enter value: ");
      int searchval=myScanner.nextInt();
      int indexval=binarysearch(array2, searchval, arraylength2);
      System.out.println(Arrays.toString(array2));
      System.out.println(indexval); 
    }
    
  }
  
  public static int [] random(int arraylength){
    Random randomGenerator=new Random();
    //new array
    int[] array;
    //length of random number
    array = new  int [arraylength];
    
    //setting numbers for array from 0 to 99
    for (int i=0; i<arraylength; i++){
      array[i]=randomGenerator.nextInt(arraylength);
    }
    
    return array;
  }
  
  public static int linearsearch(int [] array, int termnum, int arraylength){
    int indexval=-1;
    int num=0;
    for (int i=0; i<arraylength; i++){
      num=array[i];
      if (num==termnum){
        indexval=i;
        break;
      }
      else{
        indexval=-1;
      }
    }
    return indexval+1;
  }
  
  
  public static int [] ascending(int arraylength){
    Random randomGenerator=new Random();
    //new array
    int[] array;
    //length of random number
    array = new  int [arraylength];
    
    int val=randomGenerator.nextInt(arraylength);
    //setting numbers for array from 0 to 99
    for (int i=0; i<arraylength; i++){
      val+=randomGenerator.nextInt(arraylength);
      array[i]=val;
    }  
    
    return array;
  }
 
  
  public static int binarysearch(int [] array, int termnum, int arraylength){
    int value=-1;
    if (arraylength>=0){
      int middleindex=1+(arraylength-1)/2;
        if (array[middleindex]==termnum){
          value=middleindex;
        }
        if (array[middleindex]>termnum){
           value=binarysearch(array, termnum, value-1);
        }
      if (array[middleindex]<termnum){
        return binarysearch(array, termnum, value+1);
      }
      }
    return value;
  }
  
}   