//Eliza Howard Lab 05 3/1/19 CSE02 
//Making a pyramid with peak on bottom and steps on right


//importing scanner
import java.util.Scanner; 
  
//public class  
public class PatternB{
  public static void main(String args[]){
    
    //Scanner input
    Scanner myScanner;
    myScanner = new Scanner( System.in );
    
//Asking for positive integer    
    System.out.print("Enter Integer from 1 to 10:  ");
    
    //removing non integer course values
    while (myScanner.hasNextInt()==false) {
      //removing stuff that isnt an int from the loop
      String junkword=myScanner.next();  
      //reprompting for scanner value
      System.out.print("Enter Integer from 1 to 10:  ");
    }
   
   //Setting int value to integer gotten from while loop
    int number=myScanner.nextInt();
    
    //removing values greater than 10
    while (number>10) { 
       //reprompting for scanner value
      System.out.print("Enter Integer from 1 to 10:  ");
      number=myScanner.nextInt();
    }
    
    //removing values less than 0
    while (number<0) {
      //reprompting for scanner value
      System.out.print("Enter Integer from 1 to 10: ");
      number=myScanner.nextInt();
    }
    
     System.out.println("Length of base of pyramid is "+number);
    
    
    for(int j=10; j>10-number; j--) { 
      int n=0;
      for (int k=1; k<=number; k++){ 
        
        
            if (j<11-k){
              continue;
            }
          n+=1;  
        System.out.print(n + " ");
            
      }
      System.out.print("\n");
      n=0;
    }   
  }
}