//Eliza Howard Lab 06 3/8/19 CSE02 
//Making a pyramid with peak on top and steps on right


//importing scanner
import java.util.Scanner; 
  
//public class  
public class PatternA{
  public static void main(String args[]){
    
    //Scanner input
    Scanner myScanner;
    myScanner = new Scanner( System.in );
    
//Asking for positive integer    
    System.out.print("Enter Integer from 1 to 10:  ");
    
    //removing non integer course values
    while (myScanner.hasNextInt()==false) {
      //removing stuff that isnt an int from the loop
      String junkword=myScanner.next();  
      //reprompting for scanner value
      System.out.print("Enter Integer from 1 to 10:  ");
    }
   
   //Setting int value to integer gotten from while loop
    int number=myScanner.nextInt();
    
    //removing values greater than 10
    while (number>10 || number<1) { 
       //reprompting for scanner value
      System.out.print("Enter Integer from 1 to 10:  ");
      number=myScanner.nextInt();
    }
    
    
     System.out.println("Length of base of pyramid is "+number);
    
    
    for(int j=0; j<number; j++) {
      for (int k=1; k<=number; k++){
        
        if (j<k-1){
          continue;
        }
        System.out.print(k + " ");
      }
      System.out.print("\n");
     }
   
    
    
    
  }
}
   