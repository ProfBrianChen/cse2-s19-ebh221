//Eliza Howard February 5, 2019
//Code used to calculate the cost of clothing items and the sales tax on them
public class Arithmetic{
  public static void main(String args[]){

//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltPrice = 33.99;

//the tax rate
double paSalesTax = 0.06;

//total cost of pants
double totalCostOfPants;   
//total cost of sweatshirts
double totalCostOfShirts;    
//total cost of belts
double totalCostOfBelts;
    
 //sales tax on pants
double SalesTaxOnPants;
//sales tax on Shirts
double SalesTaxOnShirts;
//sales tax on Belts
double SalesTaxOnBelts;
    
//total cost of purchase without sales tax
 double CostOfPurchase;
//total sales tax on purchase
 double TotalSalesTax;
//total cost including sales tax
 double TotalCost;

 //finding total cost of pants
 totalCostOfPants=numPants*pantsPrice; 
 //finding total cost of shirts
 totalCostOfShirts=numShirts*shirtPrice;
 //finding total cost of belts
 totalCostOfBelts=numBelts*beltPrice;
    
 //finding sales tax on pants
 SalesTaxOnPants=paSalesTax*totalCostOfPants;
 //finding sales tax on shirts
 SalesTaxOnShirts=paSalesTax*totalCostOfShirts;
 //finding sales tax on belts
 SalesTaxOnBelts=paSalesTax*totalCostOfBelts;
    
 //finding total cost of purchase without sales tax
 CostOfPurchase=totalCostOfBelts+totalCostOfShirts+totalCostOfPants;
 //finding total cost of sales tax
 TotalSalesTax=SalesTaxOnBelts+SalesTaxOnShirts+SalesTaxOnPants;
 //finding total cost of purchase including sales tax
 TotalCost=CostOfPurchase+TotalSalesTax;
 
 //changing to two decimal points
 SalesTaxOnBelts=(int)(SalesTaxOnBelts*100)/100.0;
 SalesTaxOnShirts=(int)(SalesTaxOnShirts*100)/100.0;
 SalesTaxOnPants=(int)(SalesTaxOnPants*100)/100.0;
 CostOfPurchase=(int)(CostOfPurchase*100)/100.0;
 TotalSalesTax=(int)(TotalSalesTax*100)/100.0;
 TotalCost=(int)(TotalCost*100)/100.0;
 
//Printing total cost of pants, shirts and belts without sales tax
System.out.println("Total cost of pants is " + totalCostOfPants + " dollars");
System.out.println("Total cost of pants is " + totalCostOfShirts + " dollars");
System.out.println("Total cost of pants is " + totalCostOfBelts + " dollars");
//Printing sales tax of pants, shirts and belts 
System.out.println("The Sales tax on pants is " + SalesTaxOnPants + " dollars");
System.out.println("The Sales tax on shirts is " + SalesTaxOnShirts + " dollars");
System.out.println("The Sales tax on belts is " + SalesTaxOnBelts + " dollars");
//Printing total cost of purchase before tax
System.out.println("The Total Cost of Purchase Before Tax is " + CostOfPurchase + " dollars");
//Printing total sales tax
System.out.println("The Total Sales Tax is " + TotalSalesTax + " dollars");
//Printing total cost of purchase before tax
System.out.println("The Total Cost of Purchase After Tax is " + TotalCost + " dollars");
  }   
 
}

//Eliza Howard February 5, 2019