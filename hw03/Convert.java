//Eliza Howard February 12, 2019
//Code used to convert meters to inches
import java.util.Scanner;
public class Convert{
  public static void main(String args[]){
//setting up an input
Scanner myScanner;
myScanner = new Scanner( System.in );
System.out.print("Enter value for meters in form of xx.xx: ");
double meters = myScanner.nextDouble();

//converting meters to inches
double convert=39.3700789;
double inches=meters*convert;

//making so only 4 decimal places are printed
inches = (int)(inches*10000)/10000.0000;
//printing inches value  
System.out.println(+ meters + "meters is " + inches + " inches");
      }   
 
}
//Eliza Howard February 12, 2019
//Code used to convert meters to inches