//Eliza Howard CSE02 Spring 2019
//Finding the volume of a box

//Setting up ability to get inputs
import java.util.Scanner;
public class BoxVolume{
  public static void main(String args[]){
//setting up an input
Scanner myScanner;
myScanner = new Scanner( System.in );  

//Getting value for length
System.out.print("Enter the value for length in form of xx: ");
double length = myScanner.nextDouble();
//Getting value for width
System.out.print("Enter the value for width in form of xx: ");
double width = myScanner.nextDouble();
//Getting value for width
System.out.print("Enter the value for height in form of xx: ");
double height = myScanner.nextDouble();


//finding volume of the box and setting different values
int volume=(int)(length*width*height);

//printing volume of the box 
System.out.println("The volume of the box is " + volume + "");
      }   
 
}