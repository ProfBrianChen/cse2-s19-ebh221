//Eliza Howard CSE 002 4/7/19
//Play the lottery

//importing scanner and random and arrays
import java.util.Scanner; 
import java.util.Random;
import java.util.Arrays;

//public class  
public class PlayLottery{
  public static void main(String args[]){
    
    //scanner setup
    Scanner myScanner;
    myScanner = new Scanner( System.in );
    
    //initilizing user numbers
    int[] user;
    user=new  int [5];   
    
    //asking for user numbers
    for (int i=0; i<5; i++){
    System.out.print("Enter a number between 0 and 59: ");
    user[i]=myScanner.nextInt();
    }
   
    //initilizing lottery numbers
    int[] lottery;
    lottery=new  int [5];
    //getting lottery numbers
    lottery = numbersPicked();    
    
   
    //printing out user numbers, lottery numbers, and whether or not you win
    System.out.println("Your numbers were " +Arrays.toString(user));
    System.out.println("The winning numbers are " +Arrays.toString(lottery));
   
    //finding out if the user won or not
    if (userWins(user,lottery)==true){
      //they won
      System.out.println("You win!");
    }
    else if (userWins(user,lottery)==false){
      //they didnt
      System.out.println("You lose.");
    }
    
}
      
  
  public static boolean userWins(int[] user, int[] winning){
  //returns true when user and winning are the same.
    if (user==winning){
      //if arrays are equal
      return true; 
    }
    else {
      //if not equal
      return false;
    }
    
  }

public static int [] numbersPicked(){
//generates the random numbers for the lottery without duplication.
  int[] lottery;
  //random number generator
  Random randomGenerator=new Random();
  lottery=new  int [5];
  //setting the lottery numbers
  for (int i=0; i<5; i++){
      lottery[i]=randomGenerator.nextInt(60);
    }
  //return lottery numbers
  return lottery;
}
  
  //end class
}
//Eliza Howard CSE 002 4/7/19
//Play the lottery

