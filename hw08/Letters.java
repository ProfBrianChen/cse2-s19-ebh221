//Eliza Howard CSE 002 4/7/19
//Sorting arrays

//importing 
import java.util.Random;
import java.util.Arrays;

//public class  
public class Letters{
  public static void main(String args[]){
  
    //random number generator
    Random randomGenerator=new Random();
    
    //making an array
    char[] array;
    //making length 15
    array = new  char [15];
    
    //initializing the number to be converted to char
    int number=1;
    //initializing the number that decides if capital or lowercase
    int col=1;
    
    //printing out the first phrase
    System.out.print("Random character array: ");
    
    for (int i=0; i<15; i++){
     //capital or lowercase
      col=randomGenerator.nextInt(2);
      //deciding if capital or lowercase
      switch (col){
          //lowercase
        case 0:
          number=randomGenerator.nextInt(90-65)+65;
          break;
          //capital
         case 1:
          number=randomGenerator.nextInt(122-97)+97;
          break;
      }
      //converting the number to a character
      char a=(char) number;
      //setting the array value equal to a
      array[i]=a;
      //printing out the array
      System.out.print(array[i]+ " ");
    }
   
    //initializing the arrays, the lengths of the arrays, and the values of the array
    char [] array1;
    char [] array2;
    int m=1;
    int n=1;
    array1 = new  char [15];
    array2 = new  char [15];
    
    //seperating the characters by part of the alphabet
    for (int i=0; i<array.length; i++){
      //A through M
      if ((array[i]>=65 && array[i]<=77) || (array[i]>=97 && array[i]<=109)){
        //setting new array equal to array val
        array1[n]=array[i];
        //increasing n
        n+=1;
      }
      //M through N
      else if ((array[i]>=78 && array[i]<=90) || (array[i]>=110 && array[i]<=122)){
        //setting new array equal to array val
        array2[m]=array[i];
        //increasing m
        m+=1;
      }
    }
    
    
    //sorting the arrays
    char[] AtoM=getAtoM(array1);
    char[] NtoZ=getNtoZ(array2);
    
    //printing out the arrays
    System.out.print("\n");
    System.out.println("AtoM characters: " +Arrays.toString(AtoM));
    System.out.println("NtoZ characters: " +Arrays.toString(NtoZ));
  }
  
  
  
  //sorting A to M
 public static char[] getAtoM (char[] array1){
   //sorting the arrat
   Arrays.sort(array1);
   return array1; 
   
 }
  //sorting N to Z
  public static char [] getNtoZ(char [] array2){
    //sorting the array
    Arrays.sort(array2);
    return array2;
    
  }
  
//end class  
}

//Eliza Howard CSE 002 4/7/19
//Sorting arrays