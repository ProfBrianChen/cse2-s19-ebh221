//Eliza Howard Lab 07 4/2/19 CSE02 
//Making a pyramid with peak on top and steps on right


//importing scanner
import java.util.Random;
import java.util.Scanner;
  
//public class  
public class Methods{
  public static String adjectives(){
    Random randomGenerator=new Random();
    int firstnum=randomGenerator.nextInt(10);
    String adjective="a";
    switch (firstnum){
      case 0: adjective="quick";
        break;
      case 1: adjective="fast";
        break;
      case 2: adjective="speedy";
        break;
      case 3: adjective="funny";
        break;
      case 4: adjective="happy";
        break;
      case 5: adjective="lazy";
        break;
      case 6: adjective="slow";
        break;
      case 7: adjective="big";
        break;
      case 8: adjective="small";
        break;
      case 9: adjective="nice";
        break;
    }
    
    return adjective;
  }
 
 
  public static String nouns(){
    Random randomGenerator=new Random();
    int secondnum=randomGenerator.nextInt(10);
    String noun="a";
    switch (secondnum){
      case 0: noun="fox";
        break;
      case 1: noun="cat";
        break;
      case 2: noun="dog";
        break;
      case 3: noun="bunny";
        break;
      case 4: noun="owl";
        break;
      case 5: noun="bear";
        break;
      case 6: noun="dolphin";
        break;
      case 7: noun="hamster";
        break;
      case 8: noun="snake";
        break;
      case 9: noun="giraffe";
        break;
    }
    
    return noun;
  }
  
  public static String verbs(){
    Random randomGenerator=new Random();
    int thirdnum=randomGenerator.nextInt(10);
    String verb="a";
    switch (thirdnum){
      case 0: verb="ran";
        break;
      case 1: verb="passed";
        break;
      case 2: verb="saw";
        break;
      case 3: verb="helped";
        break;
      case 4: verb="called";
        break;
      case 5: verb="heard";
        break;
      case 6: verb="scared";
        break;
      case 7: verb="saved";
        break;
      case 8: verb="hugged";
        break;
      case 9: verb="fed";
        break;
    }
    
    return verb;
  }    
  
    public static String noun2(){
    Random randomGenerator=new Random();
    int fourthnum=randomGenerator.nextInt(10);
    String noun="a";
    switch (fourthnum){
      case 0: noun="fox";
        break;
      case 1: noun="cat";
        break;
      case 2: noun="dog";
        break;
      case 3: noun="bunny";
        break;
      case 4: noun="owl";
        break;
      case 5: noun="bear";
        break;
      case 6: noun="dolphin";
        break;
      case 7: noun="hamster";
        break;
      case 8: noun="snake";
        break;
      case 9: noun="giraffe";
        break;
    }
    
    return noun;
  }
  
  public static String sentences(String subject){
    String noun=subject;
    Random randomGenerator=new Random();
    int fifthnum=randomGenerator.nextInt(2);
     
    String word="a";
    
    switch (fifthnum){
      case 0: word="it";
        break;
      case 1: word=noun;
        break;
    }
    
    System.out.println(word+ " was " +adjectives()+ " to "  +noun2()+ ".");
    return noun;
    
  }
  
  public static void main (String [] args){
    Scanner myScanner=new Scanner(System.in);
    String adjective1=adjectives();
    String noun1=nouns();
    String verb=verbs();
    String noun2=noun2();
    String adjective2=adjectives();
    
    System.out.println("The " +adjective1+ " " +noun1+ " " +verb+ " the " +adjective2+ " " +noun2+ ".");
    System.out.print("Would you like another sentence? If yes, enter 1.");
    int newsentence=myScanner.nextInt();
    
    while (newsentence==1){
      String subjectfinal=sentences(noun1);
      System.out.print("Would you like another sentence? If yes, enter 1.");
      newsentence=myScanner.nextInt();
    }
    
    System.out.println("The " +noun1+ " " +verbs()+ " it's " +nouns()+ "!");
    
  }
}