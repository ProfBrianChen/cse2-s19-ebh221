//Eliza Howard CSE 002 3/24/19
//Finding the area of a shape that the user inputs

//importing scanner
import java.util.Scanner; 
  
//public class  
public class Area{
  public static void main(String args[]){
    
    //Scanner input
    Scanner myScanner;
    myScanner = new Scanner( System.in );
    
    //initilizing shapes
    String rectangle = "rectangle";
    String square = "square";
    String triangle = "triangle";
    
    
   //prompting for shapes
    System.out.print("Enter Desired Shape (rectangle, square, or triangle): ");
    
    //initilize shape
    String shape=myScanner.next();
    
    //removing inputs that don't work
    while (!shape.equals(rectangle) && !shape.equals(triangle) && !shape.equals(square)){
      //asking for new input
      System.out.print("Invalid Input. Enter Desired Shape (rectangle, square, or triangle): ");
      //resetting shape to the next input until it is one of the shapes
      shape=myScanner.next();
    }
    
    //for rectangle
    if (shape.equals(rectangle)){
      //asking for height of rectangle and checking that it is a double
      double heightr=CheckInput ("Enter the desired height of the rectangle: ", myScanner);
      //asking for width of rectangle and checking that it is a double
      double widthr=CheckInput ("Enter the desired width of the rectangle: ", myScanner);
      //finding the area
      double findarea=areaR(heightr, widthr);
      //printing area
      System.out.println("The area of the rectangle is " + findarea);
    }
    //for triangle
    else if(shape.equals(triangle)){
      //asking for base of triangle and check if it a double
      double baset=CheckInput("Enter the desired base of the triangle: ", myScanner);
      //asking for height of triangle and check if it a double
      double heightt=CheckInput("Enter the desired height of the triangle: ", myScanner);
      //finding the area
      double findarea=areaT(baset, heightt);
      //printing area
      System.out.println("The area of the triangle is " +findarea);
    }
    //for square
    else if(shape.equals(square)){
      //asking for sidelength of the square and check if it is a double
      double sidelength=CheckInput("Enter the desired sidelength of the square: ", myScanner);
      //finding the area
      double findarea=areaS(sidelength);
      //printing area
      System.out.println("The area of the square is " +findarea);
    }
   
//end area   
  }

//start second method
public static double CheckInput (String input, Scanner myScanner) {
  System.out.print(input);
  
  //removing non doubles
  while (!myScanner.hasNextDouble()){
    //removes strings
    String junkword=myScanner.next();
    //prompting for new input
    System.out.print("Invalid Input. Not a double. " + input);
  }
  //setting output to the double value
  double output=myScanner.nextDouble();
  return output;
//end checkinput
}


public static double areaR (double height, double width) {
  //finding area of rectangle
  double area=(width*height);
  return area;
}


public static double areaT (double base, double height) {
  //finding area of rectangle
  double area=(0.5*base*height);
  return area;
}
  
public static double areaS (double sidelength) {
  //finding area of rectangle
  double area=(sidelength*sidelength);
  return area;
}  
 //end class   
}
//Eliza Howard CSE 002 3/24/19
//Finding the area of a shape that the user inputs