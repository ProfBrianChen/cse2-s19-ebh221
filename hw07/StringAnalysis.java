//Eliza Howard CSE 002 3/24/19
//Making sure all the characters in a string are letters

//importing scanner
import java.util.Scanner; 
  
//public class  
public class StringAnalysis{
  public static void main(String args[]){
  
    //Scanner input
    Scanner myScanner;
    myScanner = new Scanner( System.in );
    
    //initiliazing variables
    String characters;
    String yes="yes";
    String no="no";
    String checkall;
    boolean truthValue=false;
    
    //prompting user for characters
    System.out.print("Enter string of characters: "); 
    //setting characters equal to scanner
    characters=myScanner.next();
    
    //prompting user about if they want all the characters checked
    System.out.print("Do you want to check if all the characters are letters yes/no: ");
    //setting the check equal to the answer
    checkall=myScanner.next();
    
    while(!checkall.equals(yes) && !checkall.equals(no)){
      //prompting for new input
      System.out.print("Invalid Input. Enter yes or no: ");
      //resetting checkall value
      checkall=myScanner.next();
    }
    
    System.out.println(checkall);
    
    //to check all values
    if(checkall.equals(yes)){
      //go to check value
     truthValue=charCheck(characters);
    }
    else if (checkall.equals(no)){
      //go to input check
      int numChars=inputCheck ("How many characters would you like to check?", myScanner);
      truthValue=charCheck(characters, numChars);
    }
    
    
    if (truthValue==true){
      System.out.println("All characters are letters.");
    }
    else {
      System.out.println("Not all characters are letters.");
    }
    
  //end string analysis method  
  }
  
  public static boolean charCheck (String input) {
  boolean truthValue;
    
    //initilizing c as counter
    int c=0;
    //initilizing lenght of word
    int wordn=input.length();
    
    do {
      //incrementing the letter being analyzed
      char currentChar=input.charAt(c++);
       
        if (('a' <= currentChar ) && ('z' >= currentChar)){
          //for when it is a letter
          truthValue=true;
        }
       else{
         //for when it is not a letter
         truthValue=false;
       }
      //end do statement
      } while(truthValue==true && c<wordn);

      return truthValue;
     //end first check 
    }
 
  public static boolean charCheck (String input, int wordn){
    boolean truthValue;
    //counter
    int c=0;
    //initilize lenght
    int length=input.length();
    
    if (wordn<length){
      wordn=wordn;
    }
    else {
      wordn=length;
    }
    
    do{
      //incrememnting current char
      char currentChar=input.charAt(c++);
      
      if (('a' <= currentChar ) && ('z' >= currentChar)){
          //for when it is a letter
          truthValue=true;
        }
       else{
         //for when it is not a letter
         truthValue=false;
       }
    } while((truthValue==true) && (c<wordn));
    
    return truthValue;
  //end second charcheck
  } 
  
  public static int inputCheck (String printstatement, Scanner myScanner){
  //prints the input string
    System.out.print(printstatement);
    
    while(!myScanner.hasNextInt()){
      //removing other words
      String junkword=myScanner.next();
      System.out.print("Invalid input. Enter integer. " + printstatement);
      
    }
    int output=myScanner.nextInt();
    return output;
    //end inputcheck method
  }  
  
//end class
}