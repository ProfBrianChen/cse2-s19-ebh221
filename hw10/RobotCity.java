//Eliza Howard CSE 002 4/16/19
//building a city out of 2d arrays then invading and changing the blocks

import java.util.Random;

//public class  
public class RobotCity{
  public static void main(String args[]){
  //repeating 5 times
 for (int i=0; i<5; i++){   
    buildcity();
 }
      
    
    //end main method
  }
  
  public static int [][] buildcity(){
    //random number generator
    Random randomGenerator=new Random();
    //generating the length
    int length=randomGenerator.nextInt(15-10)+10;
    //generating the width
    int height=randomGenerator.nextInt(15-10)+10;
    //initializng the array and giving it height and length
    int [][]array= new int [length][height];
    
    for (int i=0; i<length; i++){
      for (int j=0; j<height; j++){
        array[i][j]=randomGenerator.nextInt(999-100)+100;
      }
    }
    
    display(array, length, height);
    //number for invade
    int k=randomGenerator.nextInt(15-10)+10;
    invade(array, length, height, k);
    return array;
   //end buildcity 
  }
  
  //printing out array
  public static void display(int [][]array, int length, int height){
    
    for (int i=0; i<length; i++){
      for (int j=0; j<height; j++){
        System.out.printf(" "+ array[i][j]);
      }
      System.out.print("\n");
    }
   
    return;
   //end display  
  }
  
  
  //invade function
  public static int [][] invade(int array[][], int length, int height, int k){
  //random number generator
    Random randomGenerator=new Random();
    //making new array
    int [][]invadedarray= new int [length][height];  

    //making the invaded indexes
    for (int i=0; i<k; i++){
      int row=randomGenerator.nextInt(array.length);
      int column=randomGenerator.nextInt(height);
      if (invadedarray[row][column]<0){
        i--;
      }
      else {
      invadedarray[row][column]=-array[row][column];
      }
    }
    
    //filling tge rest of the array
    for (int i=0; i<length; i++){
      for (int j=0; j<height; j++){
        if (invadedarray[i][j]!=-array[i][j]){
        invadedarray[i][j]=array[i][j];}
      }
    }
    
    //printing out array and updating it
    System.out.print("\n");
    display(invadedarray, length, height);
    update(invadedarray, height);
    //returniong invaded array
    return invadedarray;
    
    //end invade
  }
  
  
  //updating invasion array
 public static int [][] update (int [][] array, int height){
   
   //moving invasion over one
   for (int i=array.length-1; i>=0; i--){
     for (int j=array[i].length-1; j>=0; j--){
       if (array[i][j]<0){
         array[i][j]=-array[i][j];
         if (j+1<height){
           array[i][j+1] = array[i][j+1] * (-1);
         }   
       }
     }
   }
   //printing out araray
  System.out.print("\n");
   display(array, array.length, height);
   //return updared array
   return array;
 }
 //end class 
}

//Eliza Howard CSE 002 4/16/19
//building a city out of 2d arrays then invading and changing the blocks