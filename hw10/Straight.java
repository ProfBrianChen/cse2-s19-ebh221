//Eliza Howard CSE 002 4/29/19
//

//importing
import java.util.Arrays;
import java.util.Random;

public class Straight{
  public static void main(String args[]){
  //random generator
    Random randomGenerator=new Random();
    //initializing percent
    double percent=0;
    
    //loop to run a million times
    for (int j=0; j<1000000; j++){
    //initializing array
    int []array=new int [52];
    int a=1;
    //creating array with 1 through 52
    for (int i=0; i<52; i++){
      array[i]=a;
      a++;
    }
    
    //shuffling the cards
    for (int i=0; i<array.length; i++) {
        //setting value to swap
	      int value1 = (int) (array.length * Math.random() );
        //setting second value to swap
	      int value2= array[value1];
        //switching values
	      array[value1] = array[i];
	      array[i] = value2;
}
    
    //initializing hand
    int []hand =new int[5];
    //getting values for hand
    hand=draw(array);
    //System.out.println(Arrays.toString(array));
    //System.out.println(Arrays.toString(hand));
    
    //getting k
    int k=3;
    //finding kth lowest cards
    int lowest=lowest(hand, k);
    if (lowest==0){
    System.out.println("error");
    }
    else {
      //System.out.println("Value: "+lowest);
    }
    //printing out array
    
      //sort array
    Arrays.sort(hand);
      //finding distance between vards
    int distance=1;
      int number=0;
    for (int i=1; i<5; i++){
      //finding distance between cards
      distance=array[i]-array[i-1];
      if (distance>1){
        //one of distances not 1
        break;
      }
      else {
        //number of distances of 1
        number++;
      }
    }
      //its a straight
   if (number==4){
     percent++;
   }   
   }
    
    //finding and printing probability
   double probability=percent/1000000;
   System.out.println("Probability is " +probability);
    //end main method
  }
  
  public static int[] draw(int []array){
    int [] hand = new int [5];
    //setting the hand
    for (int i=0; i<5; i++){
      hand[i]=array[i];
    }
    
    return hand;
   //end draw method 
  }
  
  public static int lowest(int [] hand, int k){
    
    if (k<0 || k>5){
      return 0;
    }

    Arrays.sort(hand);
    int returnval=0;
   switch (k){
     case 1:
       returnval=hand[0];
       break;
     case 2:
       returnval=hand[1];
       break;
     case 3: 
       returnval=hand[2];
       break;
     case 4:
       returnval=hand[3];
       break;
     case 5:
       returnval=hand[4];
       break;
   }
  return returnval;
  //end lowest class
  }

  
  //end class
}
